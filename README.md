# bin-ui

一套基于vue2.6的UI组件库，提供常用组件和公共样式、函数

## Docs

[document](https://wangbin3162.github.io/bin-ui/)

## install 安装

```bash
npm install bin-ui -S
```

## use 使用

使用静态引入

```
<!-- import stylesheet -->
<link rel="stylesheet" href="bin-ui/styles/index.css">
<!-- import bin-ui -->
<script src="bin-ui/bin-ui.umd.min.js"></script>
```

使用

```vue
<template>
    <b-button>测试按钮</b-button>
</template>
```

## 系统生态

***bin-ui*** 基于vue2.6的UI组件库

<a href="https://github.com/wangbin3162/bin-ui/" target="_blank">Github 仓库</a> | 
<a href="https://wangbin3162.github.io/bin-ui/" target="_blank">文档说明</a>

***bin-ace-editor*** 基于vue，代码编辑器

<a href="https://github.com/wangbin3162/bin-ace-editor/" target="_blank">Github 仓库</a> | 
<a href="https://wangbin3162.github.io/bin-ace-editor/" target="_blank">文档说明</a>

***bin-tree-org*** 树形组织图组件

<a href="https://github.com/wangbin3162/bin-tree-org/" target="_blank">Github 仓库</a> | 
<a href="https://wangbin3162.github.io/bin-tree-org" target="_blank">文档说明</a>

***bin-charts*** 基于vue、 echarts的图表封装组件

<a href="https://github.com/wangbin3162/bin-charts/" target="_blank">Github 仓库</a> | 
<a href="https://wangbin3162.github.io/bin-charts/" target="_blank">文档说明</a>

***bin-animation*** 基于vue transition钩子函数的css3动画库

<a href="https://github.com/wangbin3162/bin-animation/" target="_blank">Github 仓库</a> | 
<a href="https://wangbin3162.github.io/bin-animation/" target="_blank">文档地址</a>

***bin-keyframe-animation*** js关键帧动画库

<a href="https://github.com/wangbin3162/bin-keyframe-animation/" target="_blank">Github 仓库</a> | 
<a href="https://wangbin3162.github.io/bin-keyframe-animation/" target="_blank">预览地址</a>

***bin-admin*** 基于bin-ui的集成后台管理系统 2.0版本

<a href="https://github.com/wangbin3162/bin-admin/" target="_blank">Github 仓库</a> | 
<a href="https://wangbin3162.github.io/bin-admin/" target="_blank">预览地址</a>

***bin-data*** 基于bin-ui的数据可视化系统平台

<a href="https://github.com/wangbin3162/bin-data/" target="_blank">Github 仓库</a> | 
<a href="https://wangbin3162.github.io/bin-data/" target="_blank">预览地址</a>


